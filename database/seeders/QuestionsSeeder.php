<?php

namespace Database\Seeders;

use App\Repositories\AnswerRepository;
use App\Repositories\QuestionRepository;
use Illuminate\Database\Seeder;

class QuestionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $questions = [
           [
               'label' => 'You’re really busy at work and a colleague is telling you their life story and personal woes. You:',
               'code' =>'Q-1',
               'sort' => 1,
               'answers' => [
                   [
                       'label' => 'Don’t dare to interrupt them',
                       'score' => 40,
                   ], [
                       'label' => 'Think it’s more important to give them some of your time; work can wait',
                       'score' => 30,
                   ], [
                       'label' => 'Listen, but with only with half an ear',
                       'score' => 20,
                   ], [
                       'label' => 'Interrupt and explain that you are really busy at the moment',
                       'score' => 10,
                   ],
               ]
           ],
           [
               'label' => 'You’ve been sitting in the doctor’s waiting room for more than 25 minutes. You:',
               'code' =>'Q-2',
               'sort' => 2,
               'answers' => [
                   [
                       'label' => 'Look at your watch every two minutes',
                       'score' => 10,
                   ], [
                       'label' => 'Bubble with inner anger, but keep quiet',
                       'score' => 20,
                   ], [
                       'label' => 'Explain to other equally impatient people in the room that the doctor is always running late',
                       'score' => 30,
                   ], [
                       'label' => 'Complain in a loud voice, while tapping your foot impatiently',
                       'score' => 40,
                   ],
               ]
           ],[
               'label' => 'You’re having an animated discussion with a colleague regarding a project that you’re in charge of. You:',
               'code' =>'Q-3',
               'sort' => 3,
               'answers' => [
                   [
                       'label' => 'Don’t dare contradict them',
                       'score' => 10,
                   ], [
                       'label' => 'Think that they are obviously right',
                       'score' => 20,
                   ], [
                       'label' => 'Defend your own point of view, tooth and nail',
                       'score' => 30,
                   ], [
                       'label' => 'Continuously interrupt your colleague',
                       'score' => 40,
                   ],
               ]
           ],[
               'label' => 'You are taking part in a guided tour of a museum. You:',
               'code' =>'Q-4',
               'sort' => 4,
               'answers' => [
                   [
                       'label' => 'Are a bit too far towards the back so don’t really hear what the guide is saying',
                       'score' => 10,
                   ], [
                       'label' => 'Follow the group without question',
                       'score' => 20,
                   ], [
                       'label' => 'Make sure that everyone is able to hear properly',
                       'score' => 30,
                   ], [
                       'label' => 'Are right up the front, adding your own comments in a loud voice',
                       'score' => 40,
                   ],
               ]
           ],[
               'label' => 'During dinner parties at your home, you have a hard time with people who:',
               'code' =>'Q-5',
               'sort' => 5,
               'answers' => [
                   [
                       'label' => 'Ask you to tell a story in front of everyone else',
                       'score' => 40,
                   ], [
                       'label' => 'Talk privately between themselves',
                       'score' => 30,
                   ], [
                       'label' => 'Hang around you all evening',
                       'score' => 20,
                   ], [
                       'label' => 'Always drag the conversation back to themselves',
                       'score' => 10,
                   ],
               ]
           ],
       ];

       $questionRepo = app(QuestionRepository::class);
       $answerRepo = app(AnswerRepository::class);
       foreach ($questions as $question){
           $added = $questionRepo->updateOrCreate(['code' => $question['code']],
               ['label' => $question['label'], 'sort' => $question['sort']]);
            $answerRepo->destroy(['question_id' => $added->id]);
           foreach ($question['answers'] as $answer){
               $answerRepo->create(['question_id' => $added->id, 'label' => $answer['label'], 'score' => $answer['score']]);
           }
       }
    }
}
