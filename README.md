Teamway backend test 

<h2>Laravel Backend</h2>
 <h3> to run this project </h3>
  <p>run following command</p>
   <ul>
     <li>make sure you have <strong>database.sqlite</strong> file in database directory</li>
     <li>run <code>php artisan migrate</code></li>
     <li>run <code>php artisan db:seed --class=QuestionsSeeder</code></li>
   </ul>
   <h3> to run Unit test </h3>
    <p>run <code>php artisan test</code></p>
    
    

  
