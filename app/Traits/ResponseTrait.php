<?php


namespace App\Traits;


trait ResponseTrait
{

    public function success($data,$code = 200, $message = 'Success'){
        return response()->json([
            'status'=> 'Success',
            'code' => $code,
            'message' => $message,
            'data' => $data,
        ], $code);
    }


    public function failed($data = [],$code = 500, $message = 'Failed',$errors =[]){
        return response()->json([
            'status'=> 'Failed',
            'code' => $code,
            'message' => $message,
            'data' => $data,
            'errors' => $errors
        ], $code);
    }


}
