<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Question extends Model
{
    protected $fillable = ['label','code','sort'];

    protected $casts = ['sort' => 'integer'];

    public function answers(): HasMany{
        return $this->hasMany(Answer::class);
    }
}
