<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Answer extends Model
{
    protected $fillable = ['label','score','question_id'];

    protected $casts = ['score' => 'integer'];

    public function question(): BelongsTo{
        return $this->belongsTo(Question::class);
    }
}
