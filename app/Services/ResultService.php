<?php


namespace App\Services;


use App\Repositories\AnswerRepository;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

class ResultService
{
    protected $answerRepo;

    public function __construct( AnswerRepository $answerRepo)
    {
        $this->answerRepo = $answerRepo;
    }

    public function calculateResult($answeredQuestions){
        $answers = $this->answerRepo->findByIds(array_map(function($item){
            return $item['answer_id'];
        },$answeredQuestions));
        $result = 0;
        foreach ($answeredQuestions as $answeredQuestion){
            $currentAnswer = $answers->where('id', $answeredQuestion['answer_id'])->first();
            if($currentAnswer->question_id != $answeredQuestion['question_id']){
                throw new UnprocessableEntityHttpException('Your Answers not correct');
            }
            $result += $currentAnswer->score;
        }
        $ranges = $this->getRanges();
        $keys = array_keys($ranges);
        foreach ($keys as $key){
            if(in_array($result, $ranges[$key])){
                return strtoupper(str_replace("_"," ", $key));
            }
        }

    }

    private function getRanges(){
        return [
            "extremely_introvert" =>  range(0,50,10),
            "introvert" =>  range(60,90,10),
            "introvert_extrovert" =>  range(100,120,10),
            "extrovert" =>  range(130,160,10),
            "extremely_extrovert" =>  range(170,200,10),
        ];
    }
}
