<?php


namespace App\Services;


use App\Http\Resources\Question\QuestionResource;
use App\Repositories\AnswerRepository;
use App\Repositories\QuestionRepository;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

class QuestionService
{

    protected $questionRepo;
    protected $answerRepo;

    public function __construct(QuestionRepository $questionRepo, AnswerRepository $answerRepo)
    {
        $this->questionRepo = $questionRepo;
        $this->answerRepo = $answerRepo;
    }

    public function create($data)
    {
        DB::beginTransaction();
        try {
            $question = $this->questionRepo->create(['label' => $data['label'], 'code' => $data['code'], 'sort' => $data['sort']]);
            $questionId = $question->id;
            $answers = $data['answers'];
            $this->storeAnswers($questionId, $answers);
        } catch (\Exception $e) {
            DB::rollBack();
            throw new UnprocessableEntityHttpException();
        }
        DB::commit();
        return true;
    }

    public function update($id, $data)
    {
        DB::beginTransaction();
        $question = $this->questionRepo->findOrFail($id);
        try {
            $question->update(['label' => $data['label'], 'code' => $data['code'], 'sort' => $data['sort']]);
            $question->answers()->delete();
            $this->storeAnswers($id, $data['answers']);
        } catch (\Exception $e) {
            DB::rollBack();
            throw new UnprocessableEntityHttpException();
        }
        DB::commit();
        return true;
    }

    public function getAll()
    {
        return QuestionResource::collection($this->questionRepo->all(['*'], ['answers'], 'sort', 'asc'));
    }

    public function byId($id)
    {
        return new QuestionResource($this->questionRepo->findOrFail($id, ['answers']));
    }

    public function delete($id)
    {
        $this->questionRepo->findOrFail($id);
        return $this->questionRepo->destroy(compact('id'));
    }



    private function storeAnswers($questionId, $answers)
    {
        foreach ($answers as $answer) {
            $this->answerRepo->create(['label' => $answer['label'], 'question_id' => $questionId, 'score' => $answer['score']]);
        }
    }
}
