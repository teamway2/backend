<?php

namespace App\Providers;

use App\Models\Answer;
use App\Models\Question;
use App\Repositories\AnswerRepository;
use App\Repositories\Eloquent\EloquentAnswerRepository;
use App\Repositories\Eloquent\EloquentQuestionRepository;
use App\Repositories\QuestionRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(QuestionRepository::class, function () {
            return  new EloquentQuestionRepository(new Question());
        });
        $this->app->bind(AnswerRepository::class, function () {
            return  new EloquentAnswerRepository(new Answer());
        });
    }
}
