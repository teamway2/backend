<?php


namespace App\Http\Resources\Answer;


use Illuminate\Http\Resources\Json\JsonResource;

class AnswerResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'label' => $this->label,
            'score' => $this->score,
        ];
    }
}
