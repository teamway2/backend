<?php


namespace App\Http\Resources\Answer;


use App\Models\Answer;
use Illuminate\Http\Resources\Json\ResourceCollection;

class AnswerCollection extends ResourceCollection
{
    public function toArray($request)
    {
        return $this->collection->map(function (Answer $resource) use ($request) {
            return (new AnswerResource($resource));
        })->all();
    }
}
