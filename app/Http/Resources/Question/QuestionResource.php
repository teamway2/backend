<?php


namespace App\Http\Resources\Question;


use App\Http\Resources\Answer\AnswerResource;
use Illuminate\Http\Resources\Json\JsonResource;

class QuestionResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'label' => $this->label,
            'code' => $this->code,
            'sort' => $this->sort,
            'answers' =>  AnswerResource::collection($this->answers)
        ];
    }
}
