<?php


namespace App\Http\Resources\Question;


use App\Models\Question;
use Illuminate\Http\Resources\Json\ResourceCollection;

class QuestionCollection extends ResourceCollection
{
    public function toArray($request)
    {
        return $this->collection->map(function (Question $resource) use ($request) {
            return (new QuestionResource($resource));
        })->all();
    }
}
