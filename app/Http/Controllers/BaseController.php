<?php


namespace App\Http\Controllers;


use App\Traits\ResponseTrait;

class BaseController
{
    use ResponseTrait;
}
