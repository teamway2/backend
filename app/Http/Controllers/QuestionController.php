<?php

namespace App\Http\Controllers;

use App\Http\Requests\SubmitRequest;
use App\Http\Requests\QuestionRequest;
use App\Services\QuestionService;
use App\Services\ResultService;
use Ramsey\Uuid\Type\Integer;

class QuestionController extends BaseController
{
    private $questionService;
    private $resultService;

    public function __construct(QuestionService $questionService, ResultService $resultService)
    {
        $this->questionService = $questionService;
        $this->resultService = $resultService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return $this->success($this->questionService->getAll());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\QuestionRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(QuestionRequest $request)
    {
        $res = $this->questionService->create($request->only(['label','code','sort','answers']));
        return $res ? $this->success([],201,'Created Successfully') : $this->failed();
    }

    /**
     * Display the specified resource.
     *
     * @param  Integer  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        return $this->success($this->questionService->byId($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateQuestionRequest  $request
     * @param  Integer  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(QuestionRequest $request, $id)
    {
        $res = $this->questionService->update($id,$request->only(['label','code','sort','answers']));
        return $res ? $this->success([],202,'Updated Successfully') : $this->failed();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Integer  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $this->questionService->delete($id);
        return $this->success([],202,'Deleted Successfully');
    }


}
