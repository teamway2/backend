<?php

namespace App\Http\Controllers;

use App\Http\Requests\SubmitRequest;
use App\Services\ResultService;

class ResultController extends BaseController
{

    private $resultService;

    public function __construct(ResultService $resultService)
    {
        $this->resultService = $resultService;
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  SubmitRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function submit(SubmitRequest $request)
    {
        return $this->success(['result' => $this->resultService->calculateResult($request->input('answered_questions'))]);
    }
}
