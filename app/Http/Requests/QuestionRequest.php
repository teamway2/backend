<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class QuestionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'label' => 'required|string',
            'sort' => 'required|integer|unique:questions,sort,'.$this->question,
            'code' => 'required|string|max:5|unique:questions,code,'.$this->question,
            'answers.*.label' => 'required|string',
            'answers.*.score' => 'required|integer',
        ];
    }
}
