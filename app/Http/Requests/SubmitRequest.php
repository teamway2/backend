<?php


namespace App\Http\Requests;


use App\Repositories\QuestionRepository;
use Illuminate\Foundation\Http\FormRequest;

class SubmitRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $questionsCount = app(QuestionRepository::class)->all()->count();
        return [
            'answered_questions' => 'required|array|min:'.$questionsCount.'|max:'.$questionsCount,
            'answered_questions.*.question_id' => 'required|integer|exists:questions,id',
            'answered_questions.*.answer_id' => 'required|integer|exists:answers,id',
        ];
    }
}
