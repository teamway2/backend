<?php


namespace App\Repositories\Eloquent;


use App\Repositories\AnswerRepository;

class EloquentAnswerRepository extends EloquentBaseRepository implements AnswerRepository
{

    public function findByIds($ids){
        return $this->model->newQuery()->whereIn('id', $ids)->get();
    }
}
