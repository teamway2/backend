<?php


namespace App\Repositories;


interface AnswerRepository extends BaseRepository
{
    public function findByIds($ids);
}
