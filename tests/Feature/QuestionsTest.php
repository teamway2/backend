<?php

namespace Tests\Feature;

use App\Models\Question;

class QuestionsTest extends BaseTest
{
    /**
     * @test
     */
    public function itShouldListQuestions()
    {
        $response = $this->get(route('questions.index'));

        $response->assertStatus(200)
        ->assertJsonStructure([
            'status',
            'code',
            'message',
            'data'=> ['*' => [
                'id',
                'label',
                'code',
                'sort',
                'answers' => ['*' => [
                    'id',
                    'label',
                    'score',
                ]]
            ]]
        ])->assertJson(['status' => true]);;
    }


    /**
     * @test
     */
    public function itShouldStoreQuestion()
    {
        $lastQuestion = Question::all()->sortBy('sort')->last();
        $data =[
            'label' => 'You crack a joke at work, but nobody seems to have noticed. You:',
            'code' =>'Q-' . ($lastQuestion->sort +1),
            'sort' => $lastQuestion->sort + 1,
            'answers' => [
                [
                    'label' => 'Think it’s for the best — it was a lame joke anyway',
                    'score' => 40,
                ], [
                    'label' => 'Wait to share it with your friends after work',
                    'score' => 30,
                ], [
                    'label' => 'Try again a bit later with one of your colleagues',
                    'score' => 20,
                ], [
                    'label' => 'Keep telling it until they pay attention',
                    'score' => 10,
                ],
            ]
        ];
        $response = $this->post(route('questions.store'), $data);

        $response->assertStatus(201)
            ->assertJsonStructure([
                'status',
                'code',
                'message',
                'data'=> []
            ])->assertJson(['status' => true]);;
    }


    /**
     * @test
     */
    public function itShouldUpdateQuestion()
    {
        $data =[
            'label' => 'Updated: You crack a joke at work, but nobody seems to have noticed. You:',
            'code' =>'Q-200',
            'sort' => 200,
            'answers' => [
                [
                    'label' => 'Think it’s for the best — it was a lame joke anyway',
                    'score' => 40,
                ], [
                    'label' => 'Wait to share it with your friends after work',
                    'score' => 30,
                ], [
                    'label' => 'Try again a bit later with one of your colleagues',
                    'score' => 20,
                ], [
                    'label' => 'Keep telling it until they pay attention',
                    'score' => 10,
                ],
            ]
        ];
        $response = $this->put(route('questions.update',['question' => Question::all()->first()]), $data);

        $response->assertStatus(202)
            ->assertJsonStructure([
                'status',
                'code',
                'message',
                'data'=> []
            ])->assertJson(['status' => true]);;
    }


    /**
     * @test
     */
    public function itShouldDeleteQuestion()
    {
        $response = $this->delete(route('questions.destroy',['question' => Question::all()->first()]));

        $response->assertStatus(202)
            ->assertJsonStructure([
                'status',
                'code',
                'message',
                'data'=> []
            ])->assertJson(['status' => true]);;
    }
}
