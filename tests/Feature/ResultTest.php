<?php

namespace Tests\Feature;

use App\Models\Question;

class ResultTest extends BaseTest
{

    /**
     * @test
     */
    public function itShouldCalculateResult()
    {
        $response = $this->post(route('submit', $this->preparingTestData()));
        $response->assertStatus(200)
            ->assertJsonStructure([
                'status',
                'code',
                'message',
                'data'=> ['result']
            ])->assertJson(['status' => true]);;
    }

    private function preparingTestData(){
        $questions = Question::with('answers')->get();
        $data = [];
        foreach ($questions as $question){
            $data[] = ['question_id' => $question->id, 'answer_id' => $question->answers->first()->id];
        }
        return ['answered_questions'=> $data];
    }
}
